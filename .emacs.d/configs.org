* General Configs
** conservative scrolling
#+BEGIN_SRC emacs-lisp

  (setq scroll-conservatively 100)

#+END_SRC
** disabling the bell
#+BEGIN_SRC emacs-lisp

(setq ring-bell-function 'ignore)

#+END_SRC
** highlight mode
#+BEGIN_SRC emacs-lisp

(global-hl-line-mode 1)

#+END_SRC
** Bars
#+BEGIN_SRC emacs-lisp
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
#+END_SRC
** Electric Pairs
#+BEGIN_SRC emacs-lisp
  (setq electric-pair-pairs
        '(
          (?\" . ?\")
          (?\{ . ?\})
          (?\( . ?\))
          (?\[ . ?\])))
  (electric-pair-mode 1)

#+END_SRC
** want keybinding
#+BEGIN_SRC emacs-lisp
  (setq evil-want-ketbinding nil)

#+END_SRC
** Custom Splash Screen
#+BEGIN_SRC emacs-lisp
  (use-package dashboard
      :ensure t
      :diminish dashboard-mode
      :config
      (setq dashboard-banner-logo-title "Welcome to Emacs!")
      (setq dashboard-items '((recents  . 10)
			      (bookmarks . 10)))
      (dashboard-setup-startup-hook))

#+END_SRC
* Linum
** Install
#+BEGIN_SRC emacs-lisp
(use-package linum-relative
  :ensure t
  :init
  (global-linum-mode 1)
  (require 'linum-relative)
  (linum-relative-mode 1)
  )
#+END_SRC
** Disable in pdf-view
#+BEGIN_SRC emacs-lisp
(add-hook 'pdf-view-mode-hook (lambda () (linum-mode -1)))

#+END_SRC
* Terminal
** setting the default shell to bash
#+BEGIN_SRC emacs-lisp
  (defvar my-term-shell "/bin/bash")
  (defadvice ansi-term (before force-bash)
    (interactive (list my-term-shell)))
  (ad-activate 'ansi-term)
#+END_SRC
** Changing yes/no to y/n
#+BEGIN_SRC emacs-lisp

  (defalias 'yes-or-no-p 'y-or-n-p)

#+END_SRC
* Org 
** Install
#+BEGIN_SRC emacs-lisp
(use-package org
  :ensure t)


#+END_SRC
** Bullets
#+BEGIN_SRC emacs-lisp
  (use-package org-bullets
    :ensure t
    :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))
#+END_SRC
** fixing C-c '
#+BEGIN_SRC emacs-lisp
  (setq org-src-window-setup 'current-window)

#+END_SRC
* Which key
#+BEGIN_SRC emacs-lisp
(use-package which-key
  :ensure t
  :init
  (which-key-mode))


#+END_SRC
* Evil Mode
** Install Evil
#+BEGIN_SRC emacs-lisp
(use-package evil
  :ensure t
  :init
  (evil-mode 1))

#+END_SRC
* Latex shit
** AucTex
#+BEGIN_SRC emacs-lisp
(use-package auctex
  :defer t
  :ensure t)

#+END_SRC
*** getting it to work
#+BEGIN_SRC emacs-lisp
(load "auctex.el" nil t t)
(setq TeX-auto-save t)
(setq TeX-parse-self t)

#+END_SRC
* PDF Tools
** install
#+BEGIN_SRC emacs-lisp
(use-package pdf-tools
  :ensure t)
#+END_SRC
** activate
#+BEGIN_SRC emacs-lisp
(pdf-tools-install)
#+END_SRC
** Midnight Mode
#+BEGIN_SRC emacs-lisp
  (add-hook 'pdf-view-mode-hook 'pdf-view-midnight-minor-mode)
#+END_SRC
** Fix the blinking
#+BEGIN_SRC emacs-lisp
  (blink-cursor-mode -1)
#+END_SRC
* Yasnippet
** Install
#+BEGIN_SRC emacs-lisp
(use-package yasnippet
  :ensure t
  :config (use-package yasnippet-snippets
    :ensure t)
  (yas-reload-all))
#+END_SRC
** Enabling it
#+BEGIN_SRC emacs-lisp
  (yas-global-mode 1)

#+END_SRC
** Enabling Recursive Snippets
#+BEGIN_SRC emacs-lisp
  (setq yas-triggers-in-field t)

#+END_SRC
* Helm
** Install
#+BEGIN_SRC emacs-lisp
  (use-package helm
    :ensure t)

#+END_SRC
** Config
#+BEGIN_SRC emacs-lisp
(require 'helm)
(require 'helm-config)

;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t
      helm-echo-input-in-header-line t)

(defun spacemacs//helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face
                   (let ((bg-color (face-background 'default nil)))
                     `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))


(add-hook 'helm-minibuffer-set-up-hook
          'spacemacs//helm-hide-minibuffer-maybe)

(setq helm-autoresize-max-height 0)
(setq helm-autoresize-min-height 20)
(helm-autoresize-mode 1)

(helm-mode 1)

#+END_SRC
** My Configs
*** Helm M-x
#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "M-x") 'helm-M-x)

#+END_SRC
*** Helm Find Files
#+BEGIN_SRC emacs-lisp
  (global-set-key (kbd "C-x C-f") 'helm-find-files)

#+END_SRC
